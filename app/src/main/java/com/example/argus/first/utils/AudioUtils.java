package com.example.argus.first.utils;

import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import com.example.argus.first.R;
import com.example.argus.first.StopPlayInterface;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AudioUtils {

    public static final String AUDIO_RECORDER_FOLDER = "AudioRecorder";

    private static final int DEFINITION_COEFFICIENT = 3;
    private static final int RECORDER_BPP = 16;
    private static final int SAMPLE_RATE = 44100;
    private static final String AUDIO_RECORDER_FILE_EXT_WAV = ".wav";
    private static final String AUDIO_RECORDER_TEMP_FILE = "record_temp.raw";
    private static final int CHANNEL_CONFIG = AudioFormat.CHANNEL_IN_STEREO;
    private static final int AUDIO_FORMAT = AudioFormat.ENCODING_PCM_16BIT;

    private static AudioTrack at;
    private static AudioRecord audioRecord;

    private int internalBufferSize;
    private boolean isRecording = false;

    private Thread playThread;
    private Thread recordingThread;
    private StopPlayInterface stopPlayInterface;

    public  AudioUtils() {
        int minInternalBufferSize = AudioRecord.getMinBufferSize(SAMPLE_RATE,
                CHANNEL_CONFIG, AUDIO_FORMAT);
        internalBufferSize = minInternalBufferSize * 4;
        initAudioRecord();
        audioRecord.startRecording();
    }

    private void initAudioRecord() {
        audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC,
                SAMPLE_RATE, CHANNEL_CONFIG, AUDIO_FORMAT, internalBufferSize);
    }

    private String getFilename(){
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath,AUDIO_RECORDER_FOLDER);

        if(!file.exists()){
            file.mkdirs();
        }
        return (file.getAbsolutePath() + "/" + System.currentTimeMillis() + AUDIO_RECORDER_FILE_EXT_WAV);
    }

    private String getTempFilename(){
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath,AUDIO_RECORDER_FOLDER);
        if(!file.exists()){
            file.mkdirs();
        }
        File tempFile = new File(filepath,AUDIO_RECORDER_TEMP_FILE);
        if(tempFile.exists())
            tempFile.delete();
        return (file.getAbsolutePath() + "/" + AUDIO_RECORDER_TEMP_FILE);
    }

    public  void startPlay(final File file, StopPlayInterface stop){
        playThread = new Thread(new Runnable() {
            @Override
            public void run() {
                playWav(file);
            }
        },"AudioTrack Thread");
        stopPlayInterface = stop;
        playThread.start();
    }

    public void stopPlay(){
        if(at != null) {
            if (!playThread.isInterrupted())
                playThread.interrupt();
            int i = at.getState();
            if(i == AudioRecord.STATE_INITIALIZED) {
                at.stop();
                at.release();
            }
        }
    }

    public  void startRecording(){
        if (audioRecord == null)
            initAudioRecord();
        int i = audioRecord.getState();
        if (i == AudioRecord.STATE_INITIALIZED)
            audioRecord.startRecording();
        isRecording = true;
        recordingThread = new Thread(new Runnable() {
            @Override
            public void run() {
                writeAudioDataToFile();
            }
        },"AudioRecorder Thread");
        recordingThread.start();
    }

    public File stopRecording(Context context){
        if(audioRecord != null) {
            isRecording = false;
            int i = audioRecord.getState();
            if(i == AudioRecord.STATE_INITIALIZED) {
                audioRecord.stop();
            }
            audioRecord.release();
            audioRecord = null;
            recordingThread = null;
        }
        File file = new File(getFilename());
        copyWaveFile(getTempFilename(),file.getAbsolutePath());
        deleteTempFile();
        Toast.makeText(context, R.string.file_saved, Toast.LENGTH_SHORT).show();
        return file;
    }

    private void deleteTempFile() {
        File file = new File(getTempFilename());

        file.delete();
    }
    private void copyWaveFile(String inFilename,String outFilename){
        FileInputStream in;
        FileOutputStream out;
        long totalAudioLen;
        long totalDataLen ;
        long longSampleRate = SAMPLE_RATE;
        int channels = 2;
        long byteRate = RECORDER_BPP * SAMPLE_RATE * channels/8;

        byte[] data = new byte[internalBufferSize];

        try {
            in = new FileInputStream(inFilename);
            out = new FileOutputStream(outFilename);
            totalAudioLen = in.getChannel().size();
            totalDataLen = totalAudioLen + 36;
            WriteWaveFileHeader(out, totalAudioLen, totalDataLen,
                    longSampleRate, channels, byteRate);

            while(in.read(data) != -1){
                out.write(data);
            }

            in.close();
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void WriteWaveFileHeader(
            FileOutputStream out, long totalAudioLen,
            long totalDataLen, long longSampleRate, int channels,
            long byteRate) throws IOException {
        byte[] header = new byte[44];
        header[0] = 'R';  // RIFF/WAVE header
        header[1] = 'I';
        header[2] = 'F';
        header[3] = 'F';
        header[4] = (byte) (totalDataLen & 0xff);
        header[5] = (byte) ((totalDataLen >> 8) & 0xff);
        header[6] = (byte) ((totalDataLen >> 16) & 0xff);
        header[7] = (byte) ((totalDataLen >> 24) & 0xff);
        header[8] = 'W';
        header[9] = 'A';
        header[10] = 'V';
        header[11] = 'E';
        header[12] = 'f';  // 'fmt ' chunk
        header[13] = 'm';
        header[14] = 't';
        header[15] = ' ';
        header[16] = 16;  // 4 bytes: size of 'fmt ' chunk
        header[17] = 0;
        header[18] = 0;
        header[19] = 0;
        header[20] = 1;  // format = 1
        header[21] = 0;
        header[22] = (byte) channels;
        header[23] = 0;
        header[24] = (byte) (longSampleRate & 0xff);
        header[25] = (byte) ((longSampleRate >> 8) & 0xff);
        header[26] = (byte) ((longSampleRate >> 16) & 0xff);
        header[27] = (byte) ((longSampleRate >> 24) & 0xff);
        header[28] = (byte) (byteRate & 0xff);
        header[29] = (byte) ((byteRate >> 8) & 0xff);
        header[30] = (byte) ((byteRate >> 16) & 0xff);
        header[31] = (byte) ((byteRate >> 24) & 0xff);
        header[32] = (byte) (2 * 16 / 8);  // block align
        header[33] = 0;
        header[34] = RECORDER_BPP;  // bits per sample
        header[35] = 0;
        header[36] = 'd';
        header[37] = 'a';
        header[38] = 't';
        header[39] = 'a';
        header[40] = (byte) (totalAudioLen & 0xff);
        header[41] = (byte) ((totalAudioLen >> 8) & 0xff);
        header[42] = (byte) ((totalAudioLen >> 16) & 0xff);
        header[43] = (byte) ((totalAudioLen >> 24) & 0xff);
        out.write(header, 0, 44);
    }

    private void writeAudioDataToFile(){
        byte data[] = new byte[internalBufferSize];
        String filename = getTempFilename();
        FileOutputStream os = null;
        try {
            os = new FileOutputStream(filename);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        int read;
        if(null != os){
            while(isRecording){
                read = audioRecord.read(data, 0, internalBufferSize);
                if(AudioRecord.ERROR_INVALID_OPERATION != read){
                    try {
                        os.write(data);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void playWav( File file){
        int minBufferSize = AudioTrack.getMinBufferSize(SAMPLE_RATE, CHANNEL_CONFIG, AUDIO_FORMAT);
        at = new AudioTrack(AudioManager.STREAM_MUSIC, SAMPLE_RATE, CHANNEL_CONFIG, AUDIO_FORMAT, minBufferSize, AudioTrack.MODE_STREAM);
        String filepath = file.getAbsolutePath();
        byte[] s = new byte[minBufferSize * DEFINITION_COEFFICIENT];
        List<Reverse> fileToReverse = new ArrayList<>();
        try {
            FileInputStream fin = new FileInputStream(filepath);
            DataInputStream dis = new DataInputStream(fin);
            int j;
            while(( j = dis.read(s, 0, minBufferSize * DEFINITION_COEFFICIENT)) > -1 && at != null){
                fileToReverse.add(new Reverse(s, j));
            }
            at.play();
            for (int i = fileToReverse.size() - 1; i >= 0; i--) {
                if (at.getState() != AudioTrack.STATE_INITIALIZED)
                    return;
                at.write((fileToReverse.get(i).array), 0, fileToReverse.get(i).buff);
            }
            if (at.getState() != AudioTrack.STATE_INITIALIZED)
                return;
            at.stop();
            at.release();
            dis.close();
            fin.close();
            stopPlayInterface.stopPlay();
        } catch (FileNotFoundException e) {
            // TODO
            e.printStackTrace();
        } catch (IOException e) {
            // TODO
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }
    public boolean isRecording() {
        return isRecording;
    }

    public class Reverse {
        private byte[] array;
        private int buff;

        public Reverse(byte[] array, int buff){
            this.array = array.clone();
            this.buff = buff;
        }
    }
}
