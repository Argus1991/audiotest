package com.example.argus.first.models;

import java.io.File;

public class AudioModel {

    private File file;
    private boolean isActive;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }
}
