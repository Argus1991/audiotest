package com.example.argus.first.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.argus.first.R;
import com.example.argus.first.models.AudioModel;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class AudioAdapter extends ArrayAdapter<AudioModel> {

    protected LayoutInflater inflater;
    protected int currentPlayPosition;

    public AudioAdapter(Context context, List<AudioModel> data) {
        super(context, R.layout.list_item_audio, data);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        AudioHolder holder;
        if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_item_audio, parent, false);
            holder = new AudioHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (AudioHolder) convertView.getTag();
        }

        final AudioModel model = getItem(position);
        if (model.isActive()) {
            setCurrentPlayPosition(position);
            holder.btnStart.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_pause_circle_filled_black_24dp));
        } else {
            holder.btnStart.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_play_circle_filled_black_24dp));
        }
        holder.txtName.setText(model.getFile().getName());
        return convertView;
    }

    public int getCurrentPlayPosition() {
        return currentPlayPosition;
    }

    public void setCurrentPlayPosition(int currentPlayPosition) {
        this.currentPlayPosition = currentPlayPosition;
    }

    public class AudioHolder {
        @InjectView(R.id.btnStart)
        ImageView btnStart;
        @InjectView(R.id.txtName)
        TextView txtName;

        public AudioHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
