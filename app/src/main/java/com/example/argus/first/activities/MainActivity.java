package com.example.argus.first.activities;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Window;
import android.view.WindowManager;

import com.example.argus.first.R;
import com.example.argus.first.fragments.MainFragment;


public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeActionBar();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.holo_blue_dark));
        }
        changeFragment(R.id.root, new MainFragment(), null, false, true);
    }

    private void initializeActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
    }

    private void changeFragment(int id, Fragment fragment, String tag, boolean addToBackStack, boolean add) {
        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (addToBackStack) {
            ft.setCustomAnimations(R.anim.slide_left, R.anim.slide_left_out, R.anim.slide_right_out, R.anim.slide_right);
            ft.addToBackStack(tag);
        }
        if (add) {
            ft.add(id, fragment);
        } else {
            ft.replace(id, fragment);
        }
        if (!isFinishing()) {
            ft.commitAllowingStateLoss();
        }
        getSupportFragmentManager().executePendingTransactions();
    }
}
