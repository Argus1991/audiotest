package com.example.argus.first.fragments;

import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.example.argus.first.R;
import com.example.argus.first.StopPlayInterface;
import com.example.argus.first.adapters.AudioAdapter;
import com.example.argus.first.models.AudioModel;
import com.example.argus.first.utils.AudioUtils;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnItemClick;

public class MainFragment extends Fragment implements StopPlayInterface{

    @InjectView(R.id.audioList)
    ListView audioList;

    @InjectView(R.id.btnMic)
    ImageView btnMic;

    @InjectView(R.id.progress)
    ProgressBar progressBar;

    AudioUtils audioUtils;
    AudioAdapter adapter;
    List<AudioModel> models = new ArrayList<>();
    int time = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        models.clear();
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.inject(this, view);
        adapter = new AudioAdapter(getActivity(), models);
        audioList.setAdapter(adapter);
        loadFiles();
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        if(audioUtils != null && audioUtils.isRecording())
            save();
    }

    public void save() {
        btnMic.setImageDrawable(getResources().getDrawable(R.drawable.ic_mic_black_24dp));
        AudioModel audioModel  = new AudioModel();
        audioModel.setFile(audioUtils.stopRecording(getActivity()));
        audioModel.setIsActive(false);
        models.add(audioModel);
        adapter.notifyDataSetChanged();
        time = 0;
        progressBar.setProgress(0);

    }
    public void loadFiles () {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath,AudioUtils.AUDIO_RECORDER_FOLDER);
        for (final File fileEntry : file.listFiles()) {
            AudioModel model = new AudioModel();
            model.setFile(fileEntry);
            model.setIsActive(false);
            models.add(model);
        }
        adapter.notifyDataSetChanged();
    }
    @OnClick(R.id.btnMic)
    public void startRecord() {
        if (audioUtils == null) {
            audioUtils = new AudioUtils();
        }
        if (!audioUtils.isRecording()) {
            btnMic.setImageDrawable(getResources().getDrawable(R.drawable.ic_pause_circle_filled_black_24dp));
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    time++;
                    if (time != 10 && audioUtils.isRecording()) {
                        progressBar.setProgress(10 *time);
                        handler.postDelayed(this, 1000);
                    } else if (audioUtils.isRecording()) {
                        save();
                    }
                }
            }, 1000);
            audioUtils.startRecording();
        } else {
            save();
        }
    }

    @OnItemClick(R.id.audioList)
    public void playAudio(int position) {
        if (audioUtils == null) {
            audioUtils =  new AudioUtils();
        }
        audioUtils.stopPlay();
        if (adapter.getCurrentPlayPosition() == position) {
            models.get(adapter.getCurrentPlayPosition()).setIsActive(false);
            adapter.notifyDataSetChanged();
        } else {
            if (adapter.getCurrentPlayPosition() != -1) {
                models.get(adapter.getCurrentPlayPosition()).setIsActive(false);
            }
            models.get(position).setIsActive(true);
            adapter.notifyDataSetChanged();
            audioUtils.startPlay(((AudioModel) audioList.getItemAtPosition(position)).getFile(), this);
        }
    }

    public void stopPlay() {
        models.get(adapter.getCurrentPlayPosition()).setIsActive(false);
        adapter.setCurrentPlayPosition(-1);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });
    }
}
